import UIKit

var greeting = "Hello, playground"


//          Constants and Variables
let maximumNumberOfLoginAttempts = 10
var currentLoginAttempt = 0
var x = 0.0, y = 0.0, z = 0.0

//          Type Annotations
var welcomeMessage: String
welcomeMessage = "Hello"

var red, green, blue: Double

var friendlyWelcome = "Hello!"
friendlyWelcome = "Bonjour!"

//          Printing Constants and Variables
print("The current value of friendlyWelcome is \(friendlyWelcome)")

//          Comments
// this is a comment
/* This is also a comment
but is written over multiple lines. */

//          Numeric Literals
let decimalInteger = 17
let binaryInteger = 0b10001       // 17 in binary notation
let octalInteger = 0o21           // 17 in octal notation
let hexadecimalInteger = 0x11     // 17 in hexadecimal notation

//          Type Aliases
/*
 Type aliases define an alternative name for an existing type. You define type aliases with the typealias keyword.
 */
typealias AudioSample = UInt16
var maxAmplitudeFound = AudioSample.min
print(maxAmplitudeFound)


//          Booleans
let orangesAreOrange = true
let turnipsAreDelicious = false

if turnipsAreDelicious {
    print("Mmm, tasty turnips!")
} else {
    print("Eww, turnips are horrible.")
}

let i = 1
if i == 1 {
    // this example will compile successfully
}

//          Tuples
let http404Error = (404, "Not Found")
let (statusCode, statusMessage) = http404Error
print("The status code is \(statusCode)")
print("The status message is \(statusMessage)")

//          If Statements and Forced Unwrapping
let possibleNumber = "123"
let convertedNumber = Int(possibleNumber)
if convertedNumber != nil {
    print("convertedNumber contains some integer value.")
}
if convertedNumber != nil {
    print("convertedNumber has an integer value of \(convertedNumber!).")
}

//          Error Handling
func canThrowAnError() throws {
    // this function may or may not throw an error
}
do {
    try canThrowAnError()
    // no error was thrown
} catch {
    // an error was thrown
}


